//
//  AddCOntact.swift
//  Contacts App


import Foundation
import UIKit
import iOSDropDown


var selectedCode:String?
class AddContactController:UIViewController {
    
    @IBOutlet weak var dropDown: DropDown!
    @IBOutlet weak var dropDownText: UITextField!
    @IBOutlet weak var codetext: UITextField!
    @IBOutlet weak var firstNametext: UITextField!
    @IBOutlet weak var surNametext: UITextField!
    @IBOutlet weak var phonetext: UITextField!
    @IBOutlet weak var emailtext: UITextField!
    var storedContactsList : [Contact] = []
   
    var allPhoneCountryCode : PhoneCountryCode?
    var contObject : Contact?
    var validation = Validation()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        dropDown.inputView = UIView()
        //It will Hide Keyboard tool bar
        dropDown.inputAccessoryView = UIView()
        //It will Hide the cursor
        dropDown.tintColor = .white
        
        
        let phoneCountryCodes = try! String(contentsOfFile: readFileBy(name: "dropdown", type: "json")).components(separatedBy: .newlines).joined()
        let data = phoneCountryCodes.data(using: .utf8)!
        
        do {
            
            guard let jsonArray = try? JSONSerialization.jsonObject(with: data) else {return }
            
            // print(jsonArray) // use the json here
            
            allPhoneCountryCode = try JSONDecoder().decode(PhoneCountryCode.self, from: data)
            //allPhoneCountryCode = allPhoneCountryCode!.sorted(by: { $0.name! < $1.name! })
            dropDown.optionArray = getCountryOptions(code: allPhoneCountryCode!)
            
        }catch let error as NSError {
            print(String(describing: error))
        }
        
        dropDown.didSelect{(selectedText , index ,id) in
            self.dropDownText.text = selectedText
            self.codetext.text = self.allPhoneCountryCode?[index].dial_code
            selectedCode = self.allPhoneCountryCode![index].code
            print("Selected text \(selectedText) ,selected cod \(selectedCode ) and dial code \(self.codetext.text)")
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        guard contObject != nil else {
            return
        }
        print("selectedCode \(selectedCode)")
        self.firstNametext.text = contObject?.firstName
        self.surNametext.text = contObject?.surName
        self.emailtext.text = contObject?.email
        self.phonetext.text = String(contObject?.phone ?? 0)
        self.dropDownText.text = getCountryName(code: (contObject?.contryCode)!)
        self.codetext.text = contObject?.contryDial
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        guard let name = firstNametext.text, let sur = surNametext.text, let email = emailtext.text,
              let phone = phonetext.text else {
                 return
              }
              let isValidateEmail = self.validation.validateEmailId(emailID: email)
              if (isValidateEmail == false) {
                 print("Incorrect Email")
                self.showToast(message: "Incorrect Email", font: .systemFont(ofSize: 12.0))
                 return
              }
        
              let isValidatePhone = self.validation.validaPhoneNumber(phoneNumber: phone)
              if (isValidatePhone == false) {
                 print("Incorrect Phone")
                self.showToast(message: "Incorrect Phone", font: .systemFont(ofSize: 12.0))
                 return
              }
        if (isValidateEmail == true  || self.emailtext.text == "" || isValidatePhone == true) {
                 print("All fields are correct")
                
                if  contObject != nil {
                    //edit contact
                    if let i = contactsList.firstIndex(of: contObject!) {
                        contObject?.firstName = self.firstNametext.text
                        contObject?.surName = self.surNametext.text
                        contObject?.email = self.emailtext.text
                        contObject?.contryCode = selectedCode
                        //contObject?.contryName = getCountryName(code: self.selectedCode!)
                        contObject?.contryDial = self.codetext.text
                        contObject?.phone = Int(self.phonetext.text!)!
                        contactsList[i] = contObject!
                    }
                   
                    self.showToast(message: "Contact Updated", font: .systemFont(ofSize: 12.0))
                } else {
                    //create new contact
                    let contact = Contact(firstName:  self.firstNametext.text ?? "", surName: self.surNametext.text ?? "", email: self.emailtext.text!, phone: Int(self.phonetext.text!)!, contryCode:selectedCode!, contryDial: self.codetext.text!)
                    print("********  \(contact)")
                    contactsList.append(contact)
                    
                    self.showToast(message: "Contact Added", font: .systemFont(ofSize: 12.0))
                }
                for elmU in contactsList {
                    self.storedContactsList.append(elmU)
                    
                }
                if let encoded1 = try? JSONEncoder().encode( self.storedContactsList) {
                    UserDefaults.standard.set(encoded1, forKey: "StoredContactsList")
                }
                self.navigationController?.popViewController(animated: true)
              }
       
    }
    func getCountryOptions(code : PhoneCountryCode) -> [String]{
        var options : [String] = []
        for elm in code {
            options.append(elm.name!)
        }
        return options
        
    }
    func getCountryName(code : String) -> String{
//        if code == nil || code == "" {
//            return ""
//        }
        let countryName = allPhoneCountryCode?.filter({$0.code == code })
        return (countryName?[0].name)!
        
    }
    private func readFileBy(name: String, type: String) -> String {
        guard let path = Bundle.main.path(forResource: name, ofType: type) else {
            return "Failed to find path"
        }
        
        do {
            return path
        } catch let error as NSError {
            print(String(describing: error))
        }
    }
}
