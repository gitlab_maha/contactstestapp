//
//  ContactsController.swift
//  Contacts App

//

import Foundation
import UIKit
import Contacts
import ContactsUI
import CoreData

var contactsList = [Contact]()

class ContactsController : UIViewController,UITableViewDelegate, CNContactPickerDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var deleteBtn: UIButton!
    var selectedCells = [Int]()
    var rowsChecked = [NSIndexPath]()
    var contactsToRemove = [Contact]()
    var usersList: [Contact] = []
    var updateContactsList : [Contact] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(tapAddBtn))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        deleteBtn.tag = 0
        self.updateContactsList.removeAll()
        self.usersList.removeAll()
        guard let storedObjItem = UserDefaults.standard.object(forKey: "StoredContactsList") else {return}
        usersList = try! JSONDecoder().decode([Contact].self, from: storedObjItem as! Data)
        print(usersList)
        contactsList = usersList
        contactsList.sort(by: {$0.firstName! < $1.firstName!})
        self.tableView.reloadData()
    }
    @objc func tapAddBtn (){
        let mainView = UIStoryboard(name:"Main", bundle: nil)
        let addContact : UIViewController = mainView.instantiateViewController(withIdentifier: "addContact")
        self.navigationController?.pushViewController (addContact, animated: true)
        
    }
    @IBAction func deleteAction(_ sender: Any) {
        deleteBtn.tag = 1
        if  contactsToRemove.count == 0 {
            self.showAlert(title: "Delete Contact", message: "Please select one or more to delete")
            return
        }
        self.showAlert(title: "Contact Deleted", message: "")
        let foundList = contactsList.filter { m in !contactsToRemove.contains(where: { $0.id == m.id })}
        contactsList = foundList
        for elmU in contactsList {
            
            self.updateContactsList.append(elmU)
        }
        if let encoded1 = try? JSONEncoder().encode( self.updateContactsList) {
            UserDefaults.standard.set(encoded1, forKey: "StoredContactsList")
        }
        tableView.reloadData()
        self.selectedCells.removeAll()
        self.contactsToRemove.removeAll()
        self.updateContactsList.removeAll()
        
    }
    
}
extension ContactsController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = contactsList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactCell
        
        cell.contactName.text = item.firstName ?? "" + " " + item.surName!
        if self.selectedCells.count == 0 {
            cell.accessoryType = .none
        }
        if (deleteBtn.tag == 1){
            cell.accessoryType = self.selectedCells.contains(indexPath.row) ? .checkmark : .none
            
        }else{
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        var item = contactsList[indexPath.row]
        if (deleteBtn.tag == 1){
            if self.selectedCells.contains(indexPath.row){
                let index = self.selectedCells.firstIndex(of: indexPath.row)
                self.selectedCells.remove(at: index!)
                self.contactsToRemove.remove(at: index!)
            }else {
                self.selectedCells.append(indexPath.row)
                self.contactsToRemove.append(item)
            }
            item.isChecked = !item.isChecked
            tableView.reloadRows(at: [indexPath], with: .automatic)
            print("contactsToRemove   \(contactsToRemove) contactsToRemovenumber \(contactsToRemove.count ) ")
            print ("\n selectedCells  \(selectedCells)")
        } else {
            let mainView = UIStoryboard(name:"Main", bundle: nil)
            let addContact = mainView.instantiateViewController(withIdentifier: "addContact") as! AddContactController
            addContact.contObject = contactsList[indexPath.row]
            self.navigationController?.pushViewController (addContact, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete")  { actions,indexPath in
            contactsList.remove(at: indexPath.row)
            for elmU in contactsList {
                
                self.updateContactsList.append(elmU)
            }
            if let encoded1 = try? JSONEncoder().encode( self.updateContactsList) {
                UserDefaults.standard.set(encoded1, forKey: "StoredContactsList")
            }
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        return [deleteAction]
    }
    
}
