//
//  ContryList.swift
//  Contacts App
//


import Foundation

struct PhoneCountryCodeElement:Codable,Equatable {
    
    let name, code,dial_code: String?
  
}

typealias PhoneCountryCode = [PhoneCountryCodeElement]

