//
//  ContactModel.swift
//  Contacts App
//
//

import Foundation


var contId:Int = 1
struct Contact:Codable,Equatable  {
    var firstName,surName,email:String?
    var phone :Int?
    var isChecked:Bool = false
    var contryCode: String?
    var contryDial: String?
    var id:Int
   
    
    init(firstName: String, surName: String,email: String,phone:Int,contryCode:String,contryDial:String) {
            self.firstName = firstName
            self.surName = surName
            self.phone = phone
            self.email = email
            self.contryCode = contryCode
            self.contryDial = contryDial
            self.id = contId
        
            contId += 1
        }
    
    
}
